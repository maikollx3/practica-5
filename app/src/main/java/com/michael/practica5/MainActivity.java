package com.michael.practica5;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btn_prefer, btn_nombres;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_prefer= findViewById(R.id.btn_comple);
        btn_nombres=findViewById(R.id.btn_nombre);

        btn_prefer.setOnClickListener(this);
        btn_nombres.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch(v.getId()){

            case R.id.btn_nombre :
                Intent intent1 = new Intent(MainActivity.this, Memoria_interna.class);
                startActivity(intent1);

                break;

            case R.id.btn_comple :
                Intent intent2 = new Intent (MainActivity.this, Complemento_practica5.class);
                startActivity(intent2);
                break;
        }
    }
}
